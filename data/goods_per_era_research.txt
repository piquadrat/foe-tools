Postmodern Era
Coke	395
Fertilizer	470
Rubber	450
Textiles	370
Whale Oil	240
Asbestos	340
Explosives	390
Machine Parts	230
Gasoline	230
Tinplate	290
Convenience Food	440
Ferroconcrete	250
Flavorants	320
Luxury Materials	380
Packaging	300
Genome Data	200
Industrial Filters	290
Renewable Resources	300
Semiconductors	340
Steel	380

Contemporary Era
Good	Quantity Needed
Asbestos	480
Explosives	380
Machine Parts	390
Gasoline	490
Tinplate	380
Convenience Food	400
Ferroconcrete	550
Flavorants	400
Luxury Materials	400
Packaging	600
Genome Data	400
Industrial Filters	400
Renewable Resources	500
Semiconductors	550
Steel	400
Bionics Data	400
Electromagnets	370
Gas	510
Plastics	360
Robots	380

Tomorrow
Convenience Food	410
Ferroconcrete	440
Flavorants	690
Luxury Materials	490
Packaging	420
Genome Data	750
Industrial Filters	750
Renewable Resources	550
Semiconductors	470
Steel	720
Bionics Data	565
Electromagnets	545
Gas	435
Plastics	550
Robots	500
Nutrition Research	480
Papercrete	530
Preservatives	310
Smart Materials	570
Translucent Concrete	510

The Future
Genome Data	450
Industrial Filters	360
Renewable Resources	450
Semiconductors	440
Steel	300
Bionics Data	1035
Electromagnets	840
Gas	915
Plastics	790
Robots	910
Nutrition Research	520
Papercrete	775
Preservatives	940
Smart Materials	880
Translucent Concrete	720
Algae	460
Biogeochemical Data	450
Nanoparticles	465
Purified Water	380
Superconductors	425

