#/bin/bash -x

OUTPUT_PATH=verified_data
INPUT_DB=foe.db

mkdir -p ${OUTPUT_PATH}

sqlite3  ${INPUT_DB} "SELECT tbl_name FROM sqlite_master WHERE type='table' and tbl_name not like 'sqlite_%';" | while read table_name
do
  sqlite3 -csv -cmd '.header on' ${INPUT_DB} "select * from ${table_name}" > "${OUTPUT_PATH}/${table_name}.csv"
done;

 sqlite3 -cmd '.dump' ${INPUT_DB} "" > "${OUTPUT_PATH}/${INPUT_DB}.sql"
