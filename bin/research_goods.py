import argparse
import lib.foedb

def get_args():
  parser = argparse.ArgumentParser()
  parser.add_argument('--fr',  required=True)
  parser.add_argument('--to',  required=True)
  args = parser.parse_args()
  return args

def select_era(foedb, era_str):
	cursor = foedb.cursor()
	cursor.execute('SELECT id,era from era where instr(era, ?) > 0',  (era_str, ))
	result = cursor.fetchall()
	if len(result) > 1:
		print("Which era did you mean to start from? Following eras match {era}:".format(era=era_str))
		for row in result:
			(id_from, era) = row
			print(era)
		exit(1)
	if len(result) < 1:
		print("No era matches {era}:".format(era=era_str))
	(era_id, era) = result[0]
	print("Found {era} for {search_str}".format(era=era, search_str=era_str))
	return era_id
	
	
accumulated_costs = {}
foedb = lib.foedb.FoeDb()  
args = get_args()
id_from = select_era(foedb, args.fr)
id_to = select_era(foedb, args.to)
cursor = foedb.cursor()
cursor.execute('SELECT era from era where id>? and id<=?',  (id_from, id_to))
result = cursor.fetchall()
for tuple in result:
  cursor.execute('SELECT good,amount from research_goods where era=?',  tuple)
  result = cursor.fetchall()
  for ( good, amount) in result:
    era = foedb.get_good_era(good)
    era_id = foedb.get_era_id(era)
    if era_id not in accumulated_costs:
      accumulated_costs[era_id] = {}
      accumulated_costs[era_id]['era'] = era
      accumulated_costs[era_id]['goods'] = {}
    if good not in accumulated_costs[era_id]['goods']:
      accumulated_costs[era_id]['goods'][good] = 0
    accumulated_costs[era_id]['goods'][good] = accumulated_costs[era_id]['goods'][good] + amount

era_ids = list(accumulated_costs.keys())
era_ids.sort()

for era_id in era_ids:
  print("Era: {era}".format(era=accumulated_costs[era_id]['era']))
  for good in accumulated_costs[era_id]['goods']:
    print("\t{good}: {amount}".format(good=good,  amount=accumulated_costs[era_id]['goods'][good]))
  
