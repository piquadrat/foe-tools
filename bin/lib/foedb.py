import sqlite3

SQLITE_DB_FILE="foe.db"

class FoeDb:
  def __init__(self):
    self.connection = self.open_db();
    print("Connection:")
    print(self.connection)
    
  def open_db(self):
    conn = sqlite3.connect(SQLITE_DB_FILE)
    return conn
    
  def cursor(self):
    return self.connection.cursor()
    
  def commit(self):
    return self.connection.commit()
  
  def get_good_era(self,  good):
    c = self.cursor()
    c.execute("select era from goods_era where good=?",  (good, ))
    result = c.fetchone()
    era = None
    if not result:
      print("ERROR: Good {good} not found in table 'goods_era'".format(good=good))
    else:
      era = result[0]
    return era
    
  def get_era_id(self, era):
    c = self.cursor()
    c.execute("select id from era where era=?",  (era, ))
    result = c.fetchone()
    id = None
    if not result:
      print("ERROR: Era {era} not found in table 'era'".format(era=era))
    else:
      id = result[0]
    return id
