#!/usr/bin/python

import os
import re
import datetime
import tkinter



class FoeGeImporter:
	def __init__(self):
		self.players = {}
		self.current_player = ""
		self.columns = 0
		self.scores = {}
		self.input = []
		self.old_data = {}
		self.input_filename = os.path.join(os.environ['HOME'], "/home/tmoeller/Documents/Privat/forge-of-empires/guild-expedition.csv")
		self.output_filename = self.input_filename.replace(".csv",".new.csv")
		now = datetime.datetime.now()
		now_str = now.strftime("%d-%m-%y")
		self.input_filename_new_data = self.output_filename + "." + now_str
		
		with open(self.input_filename, "r") as expedition_results_file:
			self.input = expedition_results_file.readlines()
			for line in self.input[2:]:
				line = line.strip()
				items = line.split(",")
				if not self.columns:
					self.columns = len(items)
				else:
					if self.columns != len(items):
						print("ERROR: Deviation in number of columns! Expected: %d, found: %d" % (self.columns, len(items)))
						print("ERROR: Offending line: %s" % line)
				self.players[items[0]] = 0
				self.old_data[items[0]] = ",".join(items[1:])
				self.fill_line = "0" + ",0" * (len(items) - 2)
		with open(self.input_filename_new_data, "r") as expedition_results_file:
			for line in expedition_results_file:
				line = line.strip()
				items = line.split(",")
				self.players[items[0]] = int(items[1])
		
		self.save_copy()
		
	def save_copy(self):
		now = datetime.datetime.now()
		now_str = now.strftime("%d/%m/%y")
		with open(self.output_filename, "w") as output_file:
			now = datetime.datetime.now()
			output_file.write("%s,%s\n" % (self.input[0].strip(), now.strftime("%d/%m/%y")))
			output_file.write("%s,%s\n" % (self.input[1].strip(), "encounters"))
			player_list = list(self.players.keys())
			player_list.sort(key=str.lower)
			for p in player_list:
				output_file.write("%s,%s,%d\n" % (p, self.old_data[p], self.players[p]))

f = FoeGeImporter()
