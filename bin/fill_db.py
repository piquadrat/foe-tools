import glob
import re
import lib.foedb

foedb = lib.foedb.FoeDb()

def error(text):
  print("ERROR: {err_txt}".format(err_txt=text))
  exit(1)
  
def import_eras():
  cursor = foedb.cursor()
  cursor.execute('CREATE TABLE IF NOT EXISTS era (id integer, era text PRIMARY KEY)')
  with open("data/eras.txt", "r") as eras_file:
    index = 0
    for line in eras_file:
      index = index + 1
      line = line.strip()
      print("Line: %s" % line)
      cursor.execute('REPLACE INTO era VALUES ( ?,? )',  (index,  line ))
  foedb.commit()

def import_goods():
  cursor = foedb.cursor()
  cursor.execute('CREATE TABLE IF NOT EXISTS goods_era (good text PRIMARY KEY, era text)')
  foedb.commit()
  era_good_files = glob.glob("data/goods/*")
  reFileEra = re.compile("^.*/([^/]*)\.[^\.]")
  for f in era_good_files:
    with open(f,  "r") as era_file:
      m = reFileEra.match(f)
      era = m.group(1)
      era = era.replace("_",  " ")
      cursor.execute("SELECT era FROM era WHERE era=?",  (era, ))
      (result, ) = cursor.fetchone()
      if not result == era:
        error("{filename_era} does not exist in era db.".format(filename_era=era))
      for line in era_file:
        line = line.strip()
        print("Add {era} {good}".format(era=era,  good=line))
        cursor.execute('REPLACE INTO goods_era VALUES (?,?)',  (line,  era))
  foedb.commit()

def import_research_goods():
  cursor = foedb.cursor()
  cursor.execute(''' CREATE TABLE IF NOT EXISTS research_goods (era text, good text, amount integer, PRIMARY KEY (era, good) ) ''')
  foedb.commit()
  era_good_files = glob.glob("data/research_goods/*")
  re_file_era = re.compile("^.*/([^/]*)\.[^\.]*$")
  re_goods_amount = re.compile("^([^,]*),([^,]*)$")
  for f in era_good_files:
    with open(f,  "r") as era_goods_amount_file:
      m = re_file_era.match(f)
      era = m.group(1)
      era = era.replace("_",  " ")
      cursor.execute("SELECT era FROM era WHERE era=?",  (era, ))
      try:
        (result, ) = cursor.fetchone()
        if not result == era:
          error("{filename_era} does not exist in era db.".format(filename_era=era))
      except TypeError:
          error("{filename_era} does not exist in era db.".format(filename_era=era))
        
      for line in era_goods_amount_file:
        m = re_goods_amount.match(line)
        good = m.group(1)
        amount = int(m.group(2))
        line = line.strip()
        print("Add {era} {good}".format(era=era,  good=line))
        cursor.execute('REPLACE INTO research_goods  VALUES (?,?,?)',  (era,  good,  amount))
  foedb.commit()

if __name__ == "__main__":
  import_eras()
  import_goods()
  import_research_goods()
