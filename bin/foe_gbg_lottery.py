import os
import random

TEMPLATE_TEXT_GBG_THREAD = """
GBG LOTTERY WINNERS - {date}

{stats}
{winners}
Congratulations!!!

Dear winners, please post the GB of your choice to this thread so that our award sponsors (PiQuadrat, haterade420, PETARveliki and Mags the lone) can pay your awards to you. Unclaimed awards will not be paid.
"""

TEMPLATE_TEXT_MAIN_THREAD = """
GBG LOTTERY WINNERS - {date}
Congratulations to:
{winners}
Please post the GB of your choice to the GBG Lottery thread to claim your rewards! 
"""


class Player:
    def __init__(self, name, negotiations, fights):
        self.name = name
        self.fights = fights
        self.negotiations = negotiations
        self.points = self.negotiations * 2 + self.fights
        self.participant = self.points > 0 and name[0] != "!"
        self.ticket_range_start = 0
        self.ticket_range_end = 0


class Lottery:
    def __init__(self):
        self.all_players = []
        self.date = ""
        with open(os.path.join(os.environ['HOME'],
                               ".forge-of-empires/gbg.csv"),
                  "r") as gbg_results_file:
            content = gbg_results_file.readlines()
            dates = content[0].split(",")
            self.date = dates[-2]
            for line in content[2:]:
                line = line.strip()
                print("Line: %s" % line)
                line = line.strip()
                items = line.split(",")
                self.all_players.append(Player(items[0], int(items[-2]), int(items[-1])))

    def draw(self):
        participants_lottery = []
        point_sum = 0
        for player in self.all_players:
            if player.participant:
                player.ticket_range_start = point_sum
                player.ticket_range_end = player.ticket_range_start + player.points
                participants_lottery.append(player)
                point_sum = player.ticket_range_end
        winning_ticket = random.randrange(point_sum)
        for player in participants_lottery:
            if player.ticket_range_start <= winning_ticket < player.ticket_range_end:
                return player

    def stats_text(self):
        negotiations = 0
        fights = 0
        participants = 0
        for player in self.all_players:
            if player.points > 0:
                negotiations = negotiations + player.negotiations
                fights = fights + player.fights
                participants = participants + 1
        return ("All in all %d guild-mates helped, with %d negotiations and %d fights.\n"
                % (participants, negotiations, fights))


if __name__ == "__main__":
    lottery = Lottery()
    draws = [60, 40, 10]
    winners_long = ""
    winners_short = ""
    print(lottery.stats_text())
    for i in range(0, len(draws)):
        winner = lottery.draw()
        winners_long = winners_long + "%d FPs go to %s with %d negotiations and %d fights\n" \
                                      % (draws[i], winner.name, winner.negotiations, winner.fights)
        winners_short = winners_short + "%d FPs go to %s with %d tickets\n" \
                                        % (draws[i], winner.name, winner.points)
        winner.participant = False
    print(TEMPLATE_TEXT_GBG_THREAD.format(date=lottery.date,
                                          stats=lottery.stats_text(),
                                          winners=winners_long))
    print(TEMPLATE_TEXT_MAIN_THREAD.format(date=lottery.date,
                                           winners=winners_short))
