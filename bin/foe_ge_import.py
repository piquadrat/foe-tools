#!/usr/bin/python

import os
import re
import datetime
import tkinter


class FoeGeImporter:
	def __init__(self):
		self.players = {}
		self.current_player = ""
		self.columns = 0
		self.scores = {}
		self.input = []
		self.old_data = {}
		self.input_filename = os.path.join(os.environ['HOME'], ".forge-of-empires/guild-expedition.csv")
		self.output_filename = self.input_filename.replace(".csv", ".new.csv")
		
		with open(self.input_filename,"r") as expedition_results_file:
			self.input = expedition_results_file.readlines()
			for line in self.input[2:]:
				line = line.strip()
				items = line.split(",")
				if not self.columns:
					self.columns = len(items)
				else:
					if self.columns != len(items):
						print("ERROR: Deviation in number of columns! Expected: %d, found: %d" % (self.columns, len(items)))
						print("ERROR: Offending line: %s" % line)
				self.players[items[0]] = 0
				self.old_data[items[0]] = ",".join(items[1:])
				self.fill_line = "0" + ",0" * (len(items) - 2)
				
		self.mainwindow = tkinter.Tk()
		scrollbar = tkinter.Scrollbar(self.mainwindow, takefocus = 0)
		scrollbar.grid(column=2, row=0, rowspan=3, sticky=tkinter.N+tkinter.S+tkinter.W)
		self.name_enter = tkinter.Entry(self.mainwindow)
		self.name_enter.bind("<Key>", self.name_enter_callback)
		self.name_enter.bind("<Return>", self.player_added)
		self.name_enter.grid(column=0, row=0)
		self.listbox = tkinter.Listbox(self.mainwindow, selectmode=tkinter.SINGLE, takefocus = 0)
		self.listbox.grid(column=0, row=1)
		self.points_enter = tkinter.Entry(self.mainwindow)
		self.points_enter.bind("<Return>", self.player_add_points)
		self.points_enter.bind("<Key>", self.player_add_points_keypressed)
		self.points_enter.grid(column=0, row=2)
		self.listbox_results = tkinter.Listbox(self.mainwindow, width=40, takefocus = 0)
		self.listbox_results.config(yscrollcommand=scrollbar.set)
		scrollbar.config(command=self.listbox_results.yview)
		self.listbox_results.grid(column=1, row=0, rowspan=3)
		self.name_enter.focus_set()
		self.mainwindow.mainloop()
		
	def name_enter_callback(self, event):
		normal_char_re = re.compile("[\w]")
		m = normal_char_re.match(event.char)
		subname = self.name_enter.get()
		if m:
			subname = subname + event.char
			print(self.name_enter.get() + event.char)
		else:
			print("No alnum...")
		options = []
		self.listbox.configure(state='normal')
		self.listbox.delete(0, self.listbox.size())		
		for p in self.players:
			if p.lower().find(subname.lower()) > -1:
				options.append(p)
		self.listbox.insert(0, *options)
		self.listbox.configure(state='disabled')
		if event.char=="\t":
			print("TAB, Name selected:")
			if self.listbox.size() == 1:
				self.current_player = self.listbox.get(0)
			else:
				self.current_player = self.name_enter.get()
			print("Current player in name_enter_callback: %s" % self.current_player)
			self.points_enter.focus_set()
	
	def player_listbox_focused(self, event):
		print("Listbox got focus...")
		self.listbox.activate(0)
		self.current_player = self.listbox.get(0)
		print("Current player: %s" % self.current_player)
	
	def player_selected(self, event):
		selected = list(self.listbox.curselection())
		print("Player selected...")
		if selected:
			self.current_player = self.listbox.get(selected[0])
		else:
			self.current_player = self.listbox.get(0)
		print("Current player: %s" % self.current_player)

	def player_added(self, event):
		print("Adding player %s" % self.name_enter.get())
		self.current_player = self.name_enter.get()

	def player_add_points_keypressed(self, event):
		if event.char=="\t":
			self.player_add_points(event)
	
	def player_add_points(self, event):
		print("Add points callback")
		self.scores[self.current_player] = int(self.points_enter.get())
		self.players[self.current_player] = int(self.points_enter.get())
		if self.current_player not in self.old_data.keys():
			self.old_data[self.current_player] = self.fill_line
		self.listbox_results.delete(0, self.listbox_results.size())
		for p in self.scores:
			self.listbox_results.insert(self.listbox_results.size(), p + "," + str(self.scores[p]))
		self.listbox_results.see(self.listbox_results.size()-1)
		self.current_player = ""
		self.name_enter.delete(0, len(self.name_enter.get()))
		self.name_enter.focus_set()
		self.save_copy()

	def save_copy(self):
		now = datetime.datetime.now()
		now_str = now.strftime("%d/%m/%y")
		with open(self.output_filename + "." + now_str.replace("/","-"), "w") as output_file:
			for p in self.scores:
				output_file.write("%s,%d\n" % (p, self.scores[p]))
		with open(self.output_filename, "w") as output_file:
			now = datetime.datetime.now()
			output_file.write("%s,%s\n" % (self.input[0].strip(), now.strftime("%d/%m/%y")))
			output_file.write("%s,%s\n" % (self.input[1].strip(), "encounters"))
			player_list = list(self.players.keys())
			player_list.sort(key=str.lower)
			for p in player_list:
				output_file.write("%s,%s,%d\n" % (p, self.old_data[p], self.players[p]))


if __name__ == "__main__":
	f = FoeGeImporter()
