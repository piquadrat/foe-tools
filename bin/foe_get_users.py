#!/usr/bin/python

import os
import sys
import json


OUTPUT_FILENAME = os.path.join(os.environ['HOME'], ".forge-of-empires/users.html")
LINK_PATTERN = "https://foestats.com/en/en12/players/profile/?server=en12&world=Mount%20Killmore&id="


def get_users(filename):
    with open(filename, "r") as last_gbg_results:
        gbg_data = json.loads(last_gbg_results.read())
    with open(OUTPUT_FILENAME, "w") as output_file:
        output_file.write("<html>\n<head>\n</head>\n<body>\n")
        for user in gbg_data:
            ref = LINK_PATTERN + str(user['player_id'])
            score = user['negotiationsWon'] * 2 + user['battlesWon']
            output_file.write("<a href=\"" + ref + "\">" + user['name'] + "</a>: " + str(score) + "<br />\n")
        output_file.write("\n</body>\n</html>\n")


def main():
    json_input_file = sys.argv[1]
    get_users(json_input_file)


if __name__ == "__main__":
    main()