PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE era (id integer, era text PRIMARY KEY);
INSERT INTO era VALUES(1,'Stone Age');
INSERT INTO era VALUES(2,'Bronce Age');
INSERT INTO era VALUES(4,'Iron Age');
INSERT INTO era VALUES(5,'Early Middle Ages');
INSERT INTO era VALUES(6,'High Middle Ages');
INSERT INTO era VALUES(7,'Late Middle Ages');
INSERT INTO era VALUES(8,'Colonial Age');
INSERT INTO era VALUES(9,'Industrial Age');
INSERT INTO era VALUES(10,'Progressive Era');
INSERT INTO era VALUES(11,'Modern Era');
INSERT INTO era VALUES(12,'Postmodern Era');
INSERT INTO era VALUES(13,'Contemporary Era');
INSERT INTO era VALUES(14,'Tomorrow');
INSERT INTO era VALUES(15,'The Future');
INSERT INTO era VALUES(16,'Arctic Future');
INSERT INTO era VALUES(17,'Oceanic Future');
INSERT INTO era VALUES(18,'Virtual Future');
INSERT INTO era VALUES(19,'Space Age Mars');
INSERT INTO era VALUES(20,'Space Age Asteroid Belt');
INSERT INTO era VALUES(21,'Space Age Venus');
CREATE TABLE goods_era (good text PRIMARY KEY, era text);
INSERT INTO goods_era VALUES('Gas','Contemporary Era');
INSERT INTO goods_era VALUES('Electromagnets','Contemporary Era');
INSERT INTO goods_era VALUES('Bionics Data','Contemporary Era');
INSERT INTO goods_era VALUES('Plastics','Contemporary Era');
INSERT INTO goods_era VALUES('Robots','Contemporary Era');
INSERT INTO goods_era VALUES('Genome Data','Postmodern Era');
INSERT INTO goods_era VALUES('Renewable Resources','Postmodern Era');
INSERT INTO goods_era VALUES('Semiconductors','Postmodern Era');
INSERT INTO goods_era VALUES('Steel','Postmodern Era');
INSERT INTO goods_era VALUES('Industrial Filters','Postmodern Era');
INSERT INTO goods_era VALUES('Fertilizer','Industrial Age');
INSERT INTO goods_era VALUES('Rubber','Industrial Age');
INSERT INTO goods_era VALUES('Coke','Industrial Age');
INSERT INTO goods_era VALUES('Textiles','Industrial Age');
INSERT INTO goods_era VALUES('Whale Oil','Industrial Age');
INSERT INTO goods_era VALUES('Basalt','Late Middle Ages');
INSERT INTO goods_era VALUES('Brass','Late Middle Ages');
INSERT INTO goods_era VALUES('Gunpowder','Late Middle Ages');
INSERT INTO goods_era VALUES('Silk','Late Middle Ages');
INSERT INTO goods_era VALUES('Talc','Late Middle Ages');
INSERT INTO goods_era VALUES('Wire','Colonial Age');
INSERT INTO goods_era VALUES('Coffee','Colonial Age');
INSERT INTO goods_era VALUES('Paper','Colonial Age');
INSERT INTO goods_era VALUES('Porcelain','Colonial Age');
INSERT INTO goods_era VALUES('Tar','Colonial Age');
INSERT INTO goods_era VALUES('Dye','Bronce Age');
INSERT INTO goods_era VALUES('Lumber','Bronce Age');
INSERT INTO goods_era VALUES('Marble','Bronce Age');
INSERT INTO goods_era VALUES('Stone','Bronce Age');
INSERT INTO goods_era VALUES('Wine','Bronce Age');
INSERT INTO goods_era VALUES('Alabaster','Early Middle Ages');
INSERT INTO goods_era VALUES('Gold','Early Middle Ages');
INSERT INTO goods_era VALUES('Granite','Early Middle Ages');
INSERT INTO goods_era VALUES('Honey','Early Middle Ages');
INSERT INTO goods_era VALUES('Copper','Early Middle Ages');
INSERT INTO goods_era VALUES('Ebony','Iron Age');
INSERT INTO goods_era VALUES('Iron','Iron Age');
INSERT INTO goods_era VALUES('Gems','Iron Age');
INSERT INTO goods_era VALUES('Limestone','Iron Age');
INSERT INTO goods_era VALUES('Cloth','Iron Age');
INSERT INTO goods_era VALUES('Data Crystals','Virtual Future');
INSERT INTO goods_era VALUES('Golden Rice','Virtual Future');
INSERT INTO goods_era VALUES('Cryptocash','Virtual Future');
INSERT INTO goods_era VALUES('Nanites','Virtual Future');
INSERT INTO goods_era VALUES('Tea Silk','Virtual Future');
INSERT INTO goods_era VALUES('Brick','High Middle Ages');
INSERT INTO goods_era VALUES('Herbs','High Middle Ages');
INSERT INTO goods_era VALUES('Glass','High Middle Ages');
INSERT INTO goods_era VALUES('Salt','High Middle Ages');
INSERT INTO goods_era VALUES('Ropes','High Middle Ages');
INSERT INTO goods_era VALUES('Pearls','Oceanic Future');
INSERT INTO goods_era VALUES('Artificial Scales','Oceanic Future');
INSERT INTO goods_era VALUES('Corals','Oceanic Future');
INSERT INTO goods_era VALUES('Biolight','Oceanic Future');
INSERT INTO goods_era VALUES('Plankton','Oceanic Future');
INSERT INTO goods_era VALUES('Orichalcum','Oceanic Future');
INSERT INTO goods_era VALUES('Nanowire','Arctic Future');
INSERT INTO goods_era VALUES('Transester Gas','Arctic Future');
INSERT INTO goods_era VALUES('AI Data','Arctic Future');
INSERT INTO goods_era VALUES('Paper Batteries','Arctic Future');
INSERT INTO goods_era VALUES('Bioplastics','Arctic Future');
INSERT INTO goods_era VALUES('Promethium','Arctic Future');
INSERT INTO goods_era VALUES('Translucent Concrete','Tomorrow');
INSERT INTO goods_era VALUES('Smart Materials','Tomorrow');
INSERT INTO goods_era VALUES('Papercrete','Tomorrow');
INSERT INTO goods_era VALUES('Preservatives','Tomorrow');
INSERT INTO goods_era VALUES('Nutrition Research','Tomorrow');
INSERT INTO goods_era VALUES('BioTech Crops','Space Age Mars');
INSERT INTO goods_era VALUES('Fusion Reactors','Space Age Mars');
INSERT INTO goods_era VALUES('Lubricants','Space Age Mars');
INSERT INTO goods_era VALUES('Mars Microbes','Space Age Mars');
INSERT INTO goods_era VALUES('Superalloys','Space Age Mars');
INSERT INTO goods_era VALUES('Asbestos','Progressive Era');
INSERT INTO goods_era VALUES('Gasoline','Progressive Era');
INSERT INTO goods_era VALUES('Machine Parts','Progressive Era');
INSERT INTO goods_era VALUES('Explosives','Progressive Era');
INSERT INTO goods_era VALUES('Tinplate','Progressive Era');
INSERT INTO goods_era VALUES('Bromine','Space Age Asteroid Belt');
INSERT INTO goods_era VALUES('Compound Fluid','Space Age Asteroid Belt');
INSERT INTO goods_era VALUES('Nickel','Space Age Asteroid Belt');
INSERT INTO goods_era VALUES('Patinum Crystal','Space Age Asteroid Belt');
INSERT INTO goods_era VALUES('Processed Material','Space Age Asteroid Belt');
INSERT INTO goods_era VALUES('Algae','The Future');
INSERT INTO goods_era VALUES('Biogeochemical Data','The Future');
INSERT INTO goods_era VALUES('Purified Water','The Future');
INSERT INTO goods_era VALUES('Superconductors','The Future');
INSERT INTO goods_era VALUES('Nanoparticles','The Future');
INSERT INTO goods_era VALUES('Flavorants','Modern Era');
INSERT INTO goods_era VALUES('Convenience Food','Modern Era');
INSERT INTO goods_era VALUES('Luxury Materials','Modern Era');
INSERT INTO goods_era VALUES('Ferroconcrete','Modern Era');
INSERT INTO goods_era VALUES('Packaging','Modern Era');
CREATE TABLE research_goods (era text, good text, amount integer, PRIMARY KEY (era, good) );
INSERT INTO research_goods VALUES('Contemporary Era','Asbestos',480);
INSERT INTO research_goods VALUES('Contemporary Era','Explosives',380);
INSERT INTO research_goods VALUES('Contemporary Era','Machine Parts',390);
INSERT INTO research_goods VALUES('Contemporary Era','Gasoline',490);
INSERT INTO research_goods VALUES('Contemporary Era','Tinplate',380);
INSERT INTO research_goods VALUES('Contemporary Era','Convenience Food',400);
INSERT INTO research_goods VALUES('Contemporary Era','Ferroconcrete',550);
INSERT INTO research_goods VALUES('Contemporary Era','Flavorants',400);
INSERT INTO research_goods VALUES('Contemporary Era','Luxury Materials',400);
INSERT INTO research_goods VALUES('Contemporary Era','Packaging',600);
INSERT INTO research_goods VALUES('Contemporary Era','Genome Data',400);
INSERT INTO research_goods VALUES('Contemporary Era','Industrial Filters',400);
INSERT INTO research_goods VALUES('Contemporary Era','Renewable Resources',500);
INSERT INTO research_goods VALUES('Contemporary Era','Semiconductors',550);
INSERT INTO research_goods VALUES('Contemporary Era','Steel',400);
INSERT INTO research_goods VALUES('Contemporary Era','Bionics Data',400);
INSERT INTO research_goods VALUES('Contemporary Era','Electromagnets',370);
INSERT INTO research_goods VALUES('Contemporary Era','Gas',510);
INSERT INTO research_goods VALUES('Contemporary Era','Plastics',360);
INSERT INTO research_goods VALUES('Contemporary Era','Robots',380);
INSERT INTO research_goods VALUES('Postmodern Era','Coke',395);
INSERT INTO research_goods VALUES('Postmodern Era','Fertilizer',470);
INSERT INTO research_goods VALUES('Postmodern Era','Rubber',450);
INSERT INTO research_goods VALUES('Postmodern Era','Textiles',370);
INSERT INTO research_goods VALUES('Postmodern Era','Whale Oil',240);
INSERT INTO research_goods VALUES('Postmodern Era','Asbestos',340);
INSERT INTO research_goods VALUES('Postmodern Era','Explosives',390);
INSERT INTO research_goods VALUES('Postmodern Era','Machine Parts',230);
INSERT INTO research_goods VALUES('Postmodern Era','Gasoline',230);
INSERT INTO research_goods VALUES('Postmodern Era','Tinplate',290);
INSERT INTO research_goods VALUES('Postmodern Era','Convenience Food',440);
INSERT INTO research_goods VALUES('Postmodern Era','Ferroconcrete',250);
INSERT INTO research_goods VALUES('Postmodern Era','Flavorants',320);
INSERT INTO research_goods VALUES('Postmodern Era','Luxury Materials',380);
INSERT INTO research_goods VALUES('Postmodern Era','Packaging',300);
INSERT INTO research_goods VALUES('Postmodern Era','Genome Data',200);
INSERT INTO research_goods VALUES('Postmodern Era','Industrial Filters',290);
INSERT INTO research_goods VALUES('Postmodern Era','Renewable Resources',300);
INSERT INTO research_goods VALUES('Postmodern Era','Semiconductors',340);
INSERT INTO research_goods VALUES('Postmodern Era','Steel',380);
INSERT INTO research_goods VALUES('Industrial Age','Brick',220);
INSERT INTO research_goods VALUES('Industrial Age','Glass',160);
INSERT INTO research_goods VALUES('Industrial Age','Dried Herbs',190);
INSERT INTO research_goods VALUES('Industrial Age','Rope',190);
INSERT INTO research_goods VALUES('Industrial Age','Salt',210);
INSERT INTO research_goods VALUES('Industrial Age','Basalt',170);
INSERT INTO research_goods VALUES('Industrial Age','Brass',260);
INSERT INTO research_goods VALUES('Industrial Age','Gunpowder',210);
INSERT INTO research_goods VALUES('Industrial Age','Silk',210);
INSERT INTO research_goods VALUES('Industrial Age','Talc Powder',130);
INSERT INTO research_goods VALUES('Industrial Age','Coffee',170);
INSERT INTO research_goods VALUES('Industrial Age','Paper',120);
INSERT INTO research_goods VALUES('Industrial Age','Porcelain',160);
INSERT INTO research_goods VALUES('Industrial Age','Tar',150);
INSERT INTO research_goods VALUES('Industrial Age','Wire',160);
INSERT INTO research_goods VALUES('Industrial Age','Coke',105);
INSERT INTO research_goods VALUES('Industrial Age','Fertilizer',95);
INSERT INTO research_goods VALUES('Industrial Age','Rubber',160);
INSERT INTO research_goods VALUES('Industrial Age','Textiles',100);
INSERT INTO research_goods VALUES('Industrial Age','Whale Oil',110);
INSERT INTO research_goods VALUES('Late Middle Ages','Cloth',130);
INSERT INTO research_goods VALUES('Late Middle Ages','Ebony',140);
INSERT INTO research_goods VALUES('Late Middle Ages','Jewelry',140);
INSERT INTO research_goods VALUES('Late Middle Ages','Iron',100);
INSERT INTO research_goods VALUES('Late Middle Ages','Limestone',150);
INSERT INTO research_goods VALUES('Late Middle Ages','Copper',150);
INSERT INTO research_goods VALUES('Late Middle Ages','Gold',110);
INSERT INTO research_goods VALUES('Late Middle Ages','Granite',140);
INSERT INTO research_goods VALUES('Late Middle Ages','Honey',140);
INSERT INTO research_goods VALUES('Late Middle Ages','Alabaster',200);
INSERT INTO research_goods VALUES('Late Middle Ages','Brick',60);
INSERT INTO research_goods VALUES('Late Middle Ages','Glass',80);
INSERT INTO research_goods VALUES('Late Middle Ages','Dried Herbs',80);
INSERT INTO research_goods VALUES('Late Middle Ages','Rope',120);
INSERT INTO research_goods VALUES('Late Middle Ages','Salt',100);
INSERT INTO research_goods VALUES('Late Middle Ages','Basalt',80);
INSERT INTO research_goods VALUES('Late Middle Ages','Brass',50);
INSERT INTO research_goods VALUES('Late Middle Ages','Gunpowder',50);
INSERT INTO research_goods VALUES('Late Middle Ages','Silk',40);
INSERT INTO research_goods VALUES('Late Middle Ages','Talc Powder',70);
INSERT INTO research_goods VALUES('Colonial Age','Copper',160);
INSERT INTO research_goods VALUES('Colonial Age','Gold',170);
INSERT INTO research_goods VALUES('Colonial Age','Granite',150);
INSERT INTO research_goods VALUES('Colonial Age','Honey',160);
INSERT INTO research_goods VALUES('Colonial Age','Alabaster',120);
INSERT INTO research_goods VALUES('Colonial Age','Brick',100);
INSERT INTO research_goods VALUES('Colonial Age','Glass',130);
INSERT INTO research_goods VALUES('Colonial Age','Dried Herbs',110);
INSERT INTO research_goods VALUES('Colonial Age','Rope',110);
INSERT INTO research_goods VALUES('Colonial Age','Salt',100);
INSERT INTO research_goods VALUES('Colonial Age','Basalt',160);
INSERT INTO research_goods VALUES('Colonial Age','Brass',60);
INSERT INTO research_goods VALUES('Colonial Age','Gunpowder',150);
INSERT INTO research_goods VALUES('Colonial Age','Silk',150);
INSERT INTO research_goods VALUES('Colonial Age','Talc Powder',140);
INSERT INTO research_goods VALUES('Colonial Age','Coffee',40);
INSERT INTO research_goods VALUES('Colonial Age','Paper',80);
INSERT INTO research_goods VALUES('Colonial Age','Porcelain',60);
INSERT INTO research_goods VALUES('Colonial Age','Tar',40);
INSERT INTO research_goods VALUES('Colonial Age','Wire',80);
INSERT INTO research_goods VALUES('Bronce Age','Marble',2);
INSERT INTO research_goods VALUES('Bronce Age','Lumber',2);
INSERT INTO research_goods VALUES('Early Middle Ages','Marble',22);
INSERT INTO research_goods VALUES('Early Middle Ages','Lumber',34);
INSERT INTO research_goods VALUES('Early Middle Ages','Dye',46);
INSERT INTO research_goods VALUES('Early Middle Ages','Stone',61);
INSERT INTO research_goods VALUES('Early Middle Ages','Wine',35);
INSERT INTO research_goods VALUES('Early Middle Ages','Cloth',77);
INSERT INTO research_goods VALUES('Early Middle Ages','Ebony',63);
INSERT INTO research_goods VALUES('Early Middle Ages','Jewelry',90);
INSERT INTO research_goods VALUES('Early Middle Ages','Iron',82);
INSERT INTO research_goods VALUES('Early Middle Ages','Limestone',70);
INSERT INTO research_goods VALUES('Early Middle Ages','Copper',23);
INSERT INTO research_goods VALUES('Early Middle Ages','Gold',30);
INSERT INTO research_goods VALUES('Early Middle Ages','Granite',4);
INSERT INTO research_goods VALUES('Early Middle Ages','Honey',20);
INSERT INTO research_goods VALUES('Early Middle Ages','Alabaster',14);
INSERT INTO research_goods VALUES('Iron Age','Marble',37);
INSERT INTO research_goods VALUES('Iron Age','Lumber',60);
INSERT INTO research_goods VALUES('Iron Age','Dye',27);
INSERT INTO research_goods VALUES('Iron Age','Stone',25);
INSERT INTO research_goods VALUES('Iron Age','Wine',58);
INSERT INTO research_goods VALUES('Iron Age','Cloth',11);
INSERT INTO research_goods VALUES('Iron Age','Ebony',4);
INSERT INTO research_goods VALUES('Iron Age','Jewelry',16);
INSERT INTO research_goods VALUES('Iron Age','Iron',13);
INSERT INTO research_goods VALUES('Iron Age','Limestone',3);
INSERT INTO research_goods VALUES('Virtual Future','Artificial Scales',1240);
INSERT INTO research_goods VALUES('Virtual Future','Biolight',1120);
INSERT INTO research_goods VALUES('Virtual Future','Corals',1050);
INSERT INTO research_goods VALUES('Virtual Future','Pearls',920);
INSERT INTO research_goods VALUES('Virtual Future','Plankton',1010);
INSERT INTO research_goods VALUES('Virtual Future','Cryptocash',1985);
INSERT INTO research_goods VALUES('Virtual Future','Data Crystals',1865);
INSERT INTO research_goods VALUES('Virtual Future','Golden Rice',1990);
INSERT INTO research_goods VALUES('Virtual Future','Nanites',1945);
INSERT INTO research_goods VALUES('Virtual Future','Tea Silk',1985);
INSERT INTO research_goods VALUES('Virtual Future','Promethium',2160);
INSERT INTO research_goods VALUES('Virtual Future','Orichalcum',1990);
INSERT INTO research_goods VALUES('High Middle Ages','Marble',94);
INSERT INTO research_goods VALUES('High Middle Ages','Lumber',79);
INSERT INTO research_goods VALUES('High Middle Ages','Dye',90);
INSERT INTO research_goods VALUES('High Middle Ages','Stone',66);
INSERT INTO research_goods VALUES('High Middle Ages','Wine',71);
INSERT INTO research_goods VALUES('High Middle Ages','Cloth',30);
INSERT INTO research_goods VALUES('High Middle Ages','Ebony',42);
INSERT INTO research_goods VALUES('High Middle Ages','Jewelry',10);
INSERT INTO research_goods VALUES('High Middle Ages','Iron',47);
INSERT INTO research_goods VALUES('High Middle Ages','Limestone',28);
INSERT INTO research_goods VALUES('High Middle Ages','Copper',20);
INSERT INTO research_goods VALUES('High Middle Ages','Gold',41);
INSERT INTO research_goods VALUES('High Middle Ages','Granite',60);
INSERT INTO research_goods VALUES('High Middle Ages','Honey',30);
INSERT INTO research_goods VALUES('High Middle Ages','Alabaster',20);
INSERT INTO research_goods VALUES('High Middle Ages','Brick',40);
INSERT INTO research_goods VALUES('High Middle Ages','Glass',50);
INSERT INTO research_goods VALUES('High Middle Ages','Dried Herbs',34);
INSERT INTO research_goods VALUES('High Middle Ages','Rope',4);
INSERT INTO research_goods VALUES('High Middle Ages','Salt',11);
INSERT INTO research_goods VALUES('Oceanic Future','Nutrition Research',200);
INSERT INTO research_goods VALUES('Oceanic Future','Papercrete',250);
INSERT INTO research_goods VALUES('Oceanic Future','Preservatives',320);
INSERT INTO research_goods VALUES('Oceanic Future','Smart Materials',320);
INSERT INTO research_goods VALUES('Oceanic Future','Translucent Concrete',280);
INSERT INTO research_goods VALUES('Oceanic Future','Algae',340);
INSERT INTO research_goods VALUES('Oceanic Future','Biogeochemical Data',470);
INSERT INTO research_goods VALUES('Oceanic Future','Nanoparticles',320);
INSERT INTO research_goods VALUES('Oceanic Future','Purified Water',410);
INSERT INTO research_goods VALUES('Oceanic Future','Superconductors',410);
INSERT INTO research_goods VALUES('Oceanic Future','AI Data',1360);
INSERT INTO research_goods VALUES('Oceanic Future','Bioplastics',1320);
INSERT INTO research_goods VALUES('Oceanic Future','Nanowire',1690);
INSERT INTO research_goods VALUES('Oceanic Future','Paper Batteries',1970);
INSERT INTO research_goods VALUES('Oceanic Future','Transester Gas',1460);
INSERT INTO research_goods VALUES('Oceanic Future','Artificial Scales',2520);
INSERT INTO research_goods VALUES('Oceanic Future','Biolight',2550);
INSERT INTO research_goods VALUES('Oceanic Future','Corals',2720);
INSERT INTO research_goods VALUES('Oceanic Future','Pearls',2780);
INSERT INTO research_goods VALUES('Oceanic Future','Plankton',2550);
INSERT INTO research_goods VALUES('Oceanic Future','Promethium',6840);
INSERT INTO research_goods VALUES('Oceanic Future','Orichalcum',8590);
INSERT INTO research_goods VALUES('Arctic Future','Bionics Data',480);
INSERT INTO research_goods VALUES('Arctic Future','Gas',390);
INSERT INTO research_goods VALUES('Arctic Future','Robots',420);
INSERT INTO research_goods VALUES('Arctic Future','Nutrition Research',320);
INSERT INTO research_goods VALUES('Arctic Future','Papercrete',330);
INSERT INTO research_goods VALUES('Arctic Future','Algae',690);
INSERT INTO research_goods VALUES('Arctic Future','Biogeochemical Data',720);
INSERT INTO research_goods VALUES('Arctic Future','Nanoparticles',710);
INSERT INTO research_goods VALUES('Arctic Future','Purified Water',740);
INSERT INTO research_goods VALUES('Arctic Future','Superconductors',740);
INSERT INTO research_goods VALUES('Arctic Future','AI Data',920);
INSERT INTO research_goods VALUES('Arctic Future','Bioplastics',910);
INSERT INTO research_goods VALUES('Arctic Future','Nanowire',910);
INSERT INTO research_goods VALUES('Arctic Future','Paper Batteries',760);
INSERT INTO research_goods VALUES('Arctic Future','Transester Gas',930);
INSERT INTO research_goods VALUES('Arctic Future','Promethium',3620);
INSERT INTO research_goods VALUES('Tomorrow','Convenience Food',410);
INSERT INTO research_goods VALUES('Tomorrow','Ferroconcrete',440);
INSERT INTO research_goods VALUES('Tomorrow','Flavorants',690);
INSERT INTO research_goods VALUES('Tomorrow','Luxury Materials',490);
INSERT INTO research_goods VALUES('Tomorrow','Packaging',420);
INSERT INTO research_goods VALUES('Tomorrow','Genome Data',750);
INSERT INTO research_goods VALUES('Tomorrow','Industrial Filters',750);
INSERT INTO research_goods VALUES('Tomorrow','Renewable Resources',550);
INSERT INTO research_goods VALUES('Tomorrow','Semiconductors',470);
INSERT INTO research_goods VALUES('Tomorrow','Steel',720);
INSERT INTO research_goods VALUES('Tomorrow','Bionics Data',565);
INSERT INTO research_goods VALUES('Tomorrow','Electromagnets',545);
INSERT INTO research_goods VALUES('Tomorrow','Gas',435);
INSERT INTO research_goods VALUES('Tomorrow','Plastics',550);
INSERT INTO research_goods VALUES('Tomorrow','Robots',500);
INSERT INTO research_goods VALUES('Tomorrow','Nutrition Research',480);
INSERT INTO research_goods VALUES('Tomorrow','Papercrete',530);
INSERT INTO research_goods VALUES('Tomorrow','Preservatives',310);
INSERT INTO research_goods VALUES('Tomorrow','Smart Materials',570);
INSERT INTO research_goods VALUES('Tomorrow','Translucent Concrete',510);
INSERT INTO research_goods VALUES('Space Age Mars','Cryptocash',420);
INSERT INTO research_goods VALUES('Space Age Mars','Data Crystals',510);
INSERT INTO research_goods VALUES('Space Age Mars','Golden Rice',405);
INSERT INTO research_goods VALUES('Space Age Mars','Nanites',435);
INSERT INTO research_goods VALUES('Space Age Mars','Tea Silk',400);
INSERT INTO research_goods VALUES('Space Age Mars','Superalloys',7250);
INSERT INTO research_goods VALUES('Space Age Mars','Lubricant',7250);
INSERT INTO research_goods VALUES('Space Age Mars','BioTech Crops',7250);
INSERT INTO research_goods VALUES('Space Age Mars','Mars Microbes',7300);
INSERT INTO research_goods VALUES('Space Age Mars','Fusion Reactors',7280);
INSERT INTO research_goods VALUES('Space Age Mars','Promethium',1350);
INSERT INTO research_goods VALUES('Space Age Mars','Orichalcum',1200);
INSERT INTO research_goods VALUES('Space Age Mars','Mars Ore',3500);
INSERT INTO research_goods VALUES('Progressive Era','Basalt',180);
INSERT INTO research_goods VALUES('Progressive Era','Brass',210);
INSERT INTO research_goods VALUES('Progressive Era','Gunpowder',160);
INSERT INTO research_goods VALUES('Progressive Era','Silk',130);
INSERT INTO research_goods VALUES('Progressive Era','Talc Powder',320);
INSERT INTO research_goods VALUES('Progressive Era','Coffee',320);
INSERT INTO research_goods VALUES('Progressive Era','Paper',220);
INSERT INTO research_goods VALUES('Progressive Era','Porcelain',380);
INSERT INTO research_goods VALUES('Progressive Era','Tar',320);
INSERT INTO research_goods VALUES('Progressive Era','Wire',250);
INSERT INTO research_goods VALUES('Progressive Era','Coke',180);
INSERT INTO research_goods VALUES('Progressive Era','Fertilizer',275);
INSERT INTO research_goods VALUES('Progressive Era','Rubber',260);
INSERT INTO research_goods VALUES('Progressive Era','Textiles',230);
INSERT INTO research_goods VALUES('Progressive Era','Whale Oil',220);
INSERT INTO research_goods VALUES('Progressive Era','Asbestos',330);
INSERT INTO research_goods VALUES('Progressive Era','Explosives',300);
INSERT INTO research_goods VALUES('Progressive Era','Machine Parts',330);
INSERT INTO research_goods VALUES('Progressive Era','Gasoline',300);
INSERT INTO research_goods VALUES('Progressive Era','Tinplate',350);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Superalloys',2750);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Lubricant',2750);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','BioTech Crops',2750);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Mars Microbes',2700);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Fusion Reactors',2720);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Bromine',10000);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Compound Fluids',10000);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Nickel',10000);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Platinum Crystals',10000);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Processed Materials',10000);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Promethium',2710);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Orichalcum',3420);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Mars Ore',11500);
INSERT INTO research_goods VALUES('Space Age Asteroid Belt','Asteroid Ice',11000);
INSERT INTO research_goods VALUES('The Future','Genome Data',450);
INSERT INTO research_goods VALUES('The Future','Industrial Filters',360);
INSERT INTO research_goods VALUES('The Future','Renewable Resources',450);
INSERT INTO research_goods VALUES('The Future','Semiconductors',440);
INSERT INTO research_goods VALUES('The Future','Steel',300);
INSERT INTO research_goods VALUES('The Future','Bionics Data',1035);
INSERT INTO research_goods VALUES('The Future','Electromagnets',840);
INSERT INTO research_goods VALUES('The Future','Gas',915);
INSERT INTO research_goods VALUES('The Future','Plastics',790);
INSERT INTO research_goods VALUES('The Future','Robots',910);
INSERT INTO research_goods VALUES('The Future','Nutrition Research',520);
INSERT INTO research_goods VALUES('The Future','Papercrete',775);
INSERT INTO research_goods VALUES('The Future','Preservatives',940);
INSERT INTO research_goods VALUES('The Future','Smart Materials',880);
INSERT INTO research_goods VALUES('The Future','Translucent Concrete',720);
INSERT INTO research_goods VALUES('The Future','Algae',460);
INSERT INTO research_goods VALUES('The Future','Biogeochemical Data',450);
INSERT INTO research_goods VALUES('The Future','Nanoparticles',465);
INSERT INTO research_goods VALUES('The Future','Purified Water',380);
INSERT INTO research_goods VALUES('The Future','Superconductors',425);
INSERT INTO research_goods VALUES('Modern Era','Coffee',90);
INSERT INTO research_goods VALUES('Modern Era','Paper',40);
INSERT INTO research_goods VALUES('Modern Era','Porcelain',90);
INSERT INTO research_goods VALUES('Modern Era','Tar',130);
INSERT INTO research_goods VALUES('Modern Era','Wire',90);
INSERT INTO research_goods VALUES('Modern Era','Coke',320);
INSERT INTO research_goods VALUES('Modern Era','Fertilizer',160);
INSERT INTO research_goods VALUES('Modern Era','Rubber',130);
INSERT INTO research_goods VALUES('Modern Era','Textiles',300);
INSERT INTO research_goods VALUES('Modern Era','Whale Oil',430);
INSERT INTO research_goods VALUES('Modern Era','Asbestos',250);
INSERT INTO research_goods VALUES('Modern Era','Explosives',330);
INSERT INTO research_goods VALUES('Modern Era','Machine Parts',450);
INSERT INTO research_goods VALUES('Modern Era','Gasoline',380);
INSERT INTO research_goods VALUES('Modern Era','Tinplate',340);
INSERT INTO research_goods VALUES('Modern Era','Convenience Food',350);
INSERT INTO research_goods VALUES('Modern Era','Ferroconcrete',360);
INSERT INTO research_goods VALUES('Modern Era','Flavorants',190);
INSERT INTO research_goods VALUES('Modern Era','Luxury Materials',330);
INSERT INTO research_goods VALUES('Modern Era','Packaging',280);
COMMIT;
