# Forge of Empire Helper
## Intention
There are lots of resources available to look up information on Forge of Empire. 
Unfortunately, these resources are usually not in a shape to process them with scripts.
E.g. goods names are slightly off, "Gasoline" named "Petroleum" in other wikis, "Nutrition Research"
is called "Nutrition***al*** Research" in some areas, etc.

Mainly, this repository contains structured, consistent data to make automated processing easier,
On top of that there are some scripts to help me with my planning - if they help anyone else
as well, all the better.

## Requirements
I'm using this on Linux, but it should also work on MacOS or Windows.

* python3
* sqlite3
* bash shell 

## Getting Started
The data is stored in 2 different formats in the repository, .sql and .csv. The scripts
work on an sqlite3 database imported from the .sql file, the csv-files are for 
anyones convenience who might want there own scripts or open files in excel tables.
